package pt.ipp.isep.dei.examples.tdd.basic.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @Test
    void ensureThereAreElementAboveAverage() {
        //Arrange
        Vector inputVector =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});
        Vector expected = new Vector(new int[]{5, 6});

        //Act
        Vector result = inputVector.elementsAboveAverage();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void ensureTheSameObjectIsEqual() {
        Vector inputVector =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});

        boolean result = inputVector.equals(inputVector);

        assertTrue(result);
    }

    @Test
    void ensureVectorIsNotEqualToNull() {
        Vector inputVector =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});

        boolean result = inputVector.equals(null);

        assertFalse(result);

    }

    @Test
    void ensureVectorIsNotEqualToObject() {
        Vector inputVector =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});

        boolean result = inputVector.equals(new Object());

        assertFalse(result);
    }

    @Test
    void ensureTwoVectorsWithSameValuesAreEqual() {
        Vector vectorOne =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});

        Vector vectorTwo =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});

        boolean result = vectorOne.equals(vectorTwo);

        assertTrue(result);
    }

    @Test
    void ensureTwoVectorsWithDifferentValuesAreNotEqual() {
        Vector vectorOne =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1, 2});

        Vector vectorTwo =
                new Vector(new int[]{1, 2, 1, 2, 1, 2, 5, 6, 1});

        boolean result = vectorOne.equals(vectorTwo);

        assertFalse(result);
    }
}