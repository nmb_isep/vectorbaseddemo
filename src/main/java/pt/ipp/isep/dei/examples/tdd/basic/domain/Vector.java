package pt.ipp.isep.dei.examples.tdd.basic.domain;

import java.util.Arrays;

public class Vector {

    private int[] array;

    public Vector() {
        this(new int[0]);
    }

    public Vector(int[] array) {
        this.array = array;
    }

    /**
     * Returns the elements of the array that are above the average.
     *
     * @return Vector
     */
    public Vector elementsAboveAverage() {
        int average = average();

        Vector elementsAboveAverage =
                copyElementsAboveAverage(average);

        return elementsAboveAverage;
    }

    private Vector copyElementsAboveAverage(int average) {
        Vector elementsAboveAverage = new Vector();
        for (int i : this.array) {
            if (i > average) {
                elementsAboveAverage.add(i);
            }
        }
        return elementsAboveAverage;
    }

    private void add(int newValue) {
        int[] newArray = new int[length() + 1];

        for (int i = 0; i < length(); i++) {
            newArray[i] = array[i];
        }

        newArray[length()] = newValue;

        array = newArray;
    }

    private int average() {
        return sum() / length();
    }

    private int length() {
        return this.array.length;
    }

    private int sum() {
        int sum = 0;
        for (int i : this.array) {
            sum += i;
        }
        return sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vector vector = (Vector) o;
        return Arrays.equals(array, vector.array);
    }
}
